<?php

declare(strict_types=1);

namespace Paneric\ADTO;

use DateTimeImmutable;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use JsonSerializable;

class ADTO implements HydratorInterface, JsonSerializable
{
    protected $scMap;
    protected $ccMap;

    protected $createdAt;
    protected $updatedAt;

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    protected function setMaps(): void//has to be in child class
    {
        $ccMap = get_object_vars($this);
        unset($ccMap['ccMap'], $ccMap['scMap']);
        $this->ccMap = array_keys($ccMap);

        foreach ($this->ccMap as $ccItem) {
            $this->scMap[] = strtolower(
                preg_replace('/([a-z])([A-Z])/', '$1_$2', $ccItem)
            );
        }
    }

    protected function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $createdAt);
    }
    protected function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $updatedAt);
    }

    public function __set($name, $value)
    {
        if ($this->ccMap === null) {
            $this->setMaps();
        }

        if (in_array($name, $this->scMap)) {
            $scKey ='';

            if (strpos($name, '_') !== false) {
                $scKey = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            }

            $this->{'set' . ucfirst($scKey)}($value);
        }

        if (in_array($name, $this->ccMap)) {
            $this->{'set' . ucfirst($name)}($value);
        }
    }

    public function hydrate(array $attributes): self
    {
        if ($this->ccMap === null) {
            $this->setMaps();
        }

        foreach ($attributes as $scKey => $value) {
            if (in_array($scKey, $this->scMap) && !in_array($value, ['', [], null])) {
                $ccKey = array_search($scKey, $this->scMap);

                $this->{'set' . ucfirst($this->ccMap[$ccKey])}($value);
            }
        }

        return $this;
    }

    public function convert(): array
    {
        if ($this->ccMap === null) {
            $this->setMaps();
        }

        $attributes = [];

        foreach ($this->ccMap as $i => $ccKey) {
            if ($this->$ccKey !== null && !in_array($ccKey, ['scMap', 'ccMap'])) {
                $attributes[$this->scMap[$i]] = $this->{'get' . ucfirst($ccKey)}();
            }
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
