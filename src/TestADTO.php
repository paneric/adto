<?php

declare(strict_types=1);

namespace Paneric\ADTO;

class TestADTO extends ADTO
{
    protected $userId;
    protected $userRef;

    public function getUserId()
    {
        return $this->userId;
    }
    public function getUserRef()
    {
        return $this->userRef;
    }

    protected function setUserId($userId): void//without typing, waiting for union
    {
        $this->userId = is_array($userId) ?
            $userId :
            (int) $userId;
    }
    protected function setUserRef(string $userRef): void
    {
        $this->userRef = $userRef;
    }
}
