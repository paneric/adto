<?php

namespace spec\Paneric\ADTO;

use Paneric\ADTO\TestADTO;
use PhpSpec\ObjectBehavior;

class TestADTOSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(TestADTO::class);
    }

    function it_hydrates()
    {
        $this->hydrate(['user_id' => '1'])->shouldReturn($this);
    }

    function it_converts_1()
    {
        $this->hydrate(['user_id' => 1, 'user_ref' => 'USER_REF', 'dupa' => 'wolowa']);
        $this->convert()->shouldReturn(['user_id' => 1, 'user_ref' => 'USER_REF']);
    }

    function it_converts_2()
    {
        $this->hydrate(['user_id' => [1], 'user_ref' => 'USER_REF', 'dupa' => 'wolowa']);
        $this->convert()->shouldReturn(['user_id' => [1], 'user_ref' => 'USER_REF']);
    }

    function it_converts_3()
    {
        $this->hydrate(['user_id' => [], 'user_ref' => 'USER_REF', 'dupa' => 'wolowa']);
        $this->convert()->shouldReturn(['user_ref' => 'USER_REF']);
    }

    function it_converts_4()
    {
        $this->hydrate(['user_id' => '', 'user_ref' => 'USER_REF', 'dupa' => 'wolowa']);
        $this->convert()->shouldReturn(['user_ref' => 'USER_REF']);
    }

    function it_sets_1()
    {
        $this->userId = 1;
        $this->userRef = 'USER_REF';
        $this->convert()->shouldReturn(['user_id' => 1, 'user_ref' => 'USER_REF']);
    }

    function it_sets_2()
    {
        $this->user_id = 1;
        $this->user_ref = 'USER_REF';
        $this->convert()->shouldReturn(['user_id' => 1, 'user_ref' => 'USER_REF']);
    }
}
